#include "FBManager.h"

#include <QMessageBox>
#include <QDebug>

#include <QFacebookConnect/fbrequest.h>
#include <QFacebookConnect/fberror.h>

#include "fbvisual.h"

FBManager::FBManager(Fbvisual& fbVisual_) :  iLoginDialog ( NULL ), fbVisual(fbVisual_) {
	iFBSession = FBSession::sessionForApplication("df51def3e750a350ddb961a70b5ab5ab", "3b86a756f77967dea4674f080fa5d345", QString());
	connect (iFBSession, SIGNAL(sessionDidLogin(FBUID)), this, SLOT(sessionDidLogin(FBUID)));
	connect (iFBSession, SIGNAL(sessionDidLogout()), this, SLOT(sessionDidLogout()));
}

FBManager::~FBManager() {
	delete iFBSession;
}

void FBManager::getOwner() const {
	FBRequest* request = FBRequest::request();
	Dictionary params;
	QString query = "SELECT uid, first_name, last_name, pic_big, status, birthday_date, contact_email, profile_url, about_me "
					"FROM user "
					"WHERE uid = " + userId;
	params["query"] = query;

    connect(request, SIGNAL(requestDidLoad(QVariant)), this, SLOT(ownerLoaded(QVariant)));
    connect(request, SIGNAL(requestFailedWithFacebookError(FBError)), this, SLOT(requestFailedWithFacebookError(FBError)));

    request->call("facebook.fql.query",params);
}

void FBManager::getFriends() const {
    FBRequest* request = FBRequest::request();
    Dictionary params;
    QString query = "SELECT uid, first_name, last_name, pic_big, status, birthday_date, contact_email, profile_url, about_me "
					"FROM user "
					"WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = " + userId + ")";
    params["query"] = query;

    connect(request, SIGNAL(requestDidLoad(QVariant)), this, SLOT(friendsLoaded(QVariant)));
    connect(request, SIGNAL(requestFailedWithFacebookError(FBError)), this, SLOT(requestFailedWithFacebookError(FBError)));

    request->call("facebook.fql.query",params);
}

void FBManager::on_actionLogin_activated() {
    if (iFBSession->resume() == false) {
        iLoginDialog = new FBLoginDialog();
        iLoginDialog->show();
    }
}

void FBManager::on_actionLogout_activated() {
    iFBSession->logout();
}

void FBManager::sessionDidLogin(FBUID aUid) {
    userId = QString::number(aUid,10);
    if (iLoginDialog) {
        iLoginDialog->deleteLater();
        iLoginDialog = NULL;
    }
    emit(managerLoggedIn());
}

void FBManager::sessionDidLogout() {
    emit(managerLoggedOut());
}

void FBManager::requestFailedWithFacebookError(const FBError& aError) {
    qDebug() << "facebook error is " << aError.code() << " - " << aError.description();
}

void FBManager::friendsLoaded(const QVariant& aContainer) {
	QList<Friend*> friends;
	if (aContainer.type() == QVariant::List) {
		QVariantList list = aContainer.toList();

		int size = list.size();
		for (int i = 0 ; i < size; ++i) {
			QVariantHash dictionary = list.at(i).toHash();
			QHashIterator<QString, QVariant> iterator(dictionary);

			Friend* f = new Friend();
			f->setUid(dictionary.value("uid").toString());
			f->setName(dictionary.value("first_name").toString());
			f->setSurname(dictionary.value("last_name").toString());
			f->setBirthDate(dictionary.value("birthday_date").toString());
			f->setPhotoUrl(dictionary.value("pic_big").toString());
			f->setContactEmail(dictionary.value("contact_email").toString());
			f->setAbout(dictionary.value("about_me").toString());
			f->setProfileUrl(dictionary.value("profile_url").toString());

			friends.push_back(f);
		}
		sender()->deleteLater();
	}

	fbVisual.friendsLoaded(friends);
}

void FBManager::ownerLoaded(const QVariant& aContainer) {
	QList<Friend*> friends;
	if (aContainer.type() == QVariant::List) {
		QVariantList list = aContainer.toList();

		int size = list.size();
		for (int i = 0 ; i < size; ++i) {
			QVariantHash dictionary = list.at(i).toHash();
			QHashIterator<QString, QVariant> iterator(dictionary);

			Friend* f = new Friend();
			f->setUid(dictionary.value("uid").toString());
			f->setName(dictionary.value("first_name").toString());
			f->setSurname(dictionary.value("last_name").toString());
			f->setBirthDate(dictionary.value("birthday_date").toString());
			f->setPhotoUrl(dictionary.value("pic_big").toString());
			f->setContactEmail(dictionary.value("contact_email").toString());
			f->setAbout(dictionary.value("about_me").toString());
			f->setProfileUrl(dictionary.value("profile_url").toString());

			friends.push_back(f);
		}
		sender()->deleteLater();
	}

	fbVisual.ownerLoaded(friends.front());
}
