#include "ChangeFriendButton.h"

#include <QPainter>
#include <QtGui/QHeaderView>

ChangeFriendButton::ChangeFriendButton(bool rotate) : pixmap(QPixmap(":/gui/resources/strzalka.png")) {
    setAcceptHoverEvents(true);
    setCacheMode(DeviceCoordinateCache);
    if(rotate) {
    	QMatrix matrix;
    	matrix.rotate(180);
    	pixmap = pixmap.transformed(matrix);
    }
}

ChangeFriendButton::~ChangeFriendButton() {
	// TODO Auto-generated destructor stub
}

QRectF ChangeFriendButton::boundingRect() const {
	return QRectF(-65, -50, 130, 100);
}

QPainterPath ChangeFriendButton::shape() const {
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

void ChangeFriendButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
	QColor dark(34, 139, 34);
	QColor light(50, 205, 50);
    bool down = option->state & QStyle::State_Sunken;
    QRectF r = boundingRect();
    QLinearGradient grad(r.topLeft(), r.bottomRight());
    grad.setColorAt(down ? 1 : 0, option->state & QStyle::State_MouseOver ? Qt::white : light);
    grad.setColorAt(down ? 0 : 1, dark);
    painter->setPen(dark);
    painter->setBrush(grad);
    painter->drawEllipse(r);
    QLinearGradient grad2(r.topLeft(), r.bottomRight());
    grad.setColorAt(down ? 1 : 0, dark);
    grad.setColorAt(down ? 0 : 1, light);
    painter->setPen(Qt::NoPen);
    painter->setBrush(grad);
    if(down)
        painter->translate(2, 2);
    painter->drawEllipse(r.adjusted(5, 5, -5, -5));
    painter->drawPixmap(-pixmap.width()/2, -pixmap.height()/2, pixmap);
}

void ChangeFriendButton::mousePressEvent(QGraphicsSceneMouseEvent *) {
	emit pressed();
	update();
}

void ChangeFriendButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *) {
	update();
}
