#include "PhotoManager.h"
#include "fbvisual.h"

#include <QDir>
#include <QFile>
#include <QDebug>

PhotoManager::PhotoManager() {
	QObject::connect(&networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(requestFinished(QNetworkReply*)));

	cachePath = QDir::homePath() + "/." + APPLICATION_NAME.toLower() + "/";
}

PhotoManager::~PhotoManager() {
	// TODO Auto-generated destructor stub
}

void PhotoManager::getPhotoByUrl(const QString& id, const QString& urlString) {
	QFileInfo fileInfo(urlString);

	if(urlString.size() == 0) {
		QPixmap* pic = new QPixmap(":/gui/resources/unknown-user-poster.gif");
		emit(photoLoaded(id, pic));
	}
	else if(QFile::exists( cachePath + fileInfo.fileName() ))  {
		QPixmap* pic = new QPixmap(cachePath + fileInfo.fileName());
		emit(photoLoaded(id, pic));
	}
	else {
		QUrl url(urlString);
		QNetworkReply* reply = networkManager.get(QNetworkRequest(urlString));
		replyOnIdMap[reply] = id;
	}
}

void PhotoManager::requestFinished(QNetworkReply* reply) {
	// sprawdzenie czy wystapil blad
	if (reply->error() == QNetworkReply::NoError) {
		QString id = replyOnIdMap[reply];
		replyOnIdMap.remove(reply);
		// czytamy dane z QNetworkReply
		QImageReader imageReader(reply);
		if(imageReader.canRead()) {
			QPixmap* pic = new QPixmap(QPixmap::fromImage(imageReader.read()));
			emit(photoLoaded(id, pic));
			saveToDisk(pic, QFileInfo(reply->url().path()).fileName());
		}
		else {
			// todo obsluga bledow
		}
	}
	else {
		// TODO bledy http
	}

	reply->deleteLater();
}

void PhotoManager::saveToDisk(QPixmap* pixmap, QString fileName) {
	QDir dir(cachePath);
	if(!dir.exists()) {
		dir.mkdir(cachePath);
	}
	pixmap->save(cachePath + fileName);
	qDebug() << "Saving pixmap: " + cachePath + fileName;
}
