#include "AboutDialog.h"

AboutDialog::AboutDialog(QWidget *parent): QDialog(parent) {
	ui.setupUi(this);

	connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	fillDescription();
}

AboutDialog::~AboutDialog() { /* empty */ }

void AboutDialog::fillDescription() {
	QString description = QString(tr("FbVisual provides posibility to explore your Facebook friends on desktop.\n"));
	ui.textDescription->setText(description);
}
