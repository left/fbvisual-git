#include "fbvisual.h"

#include <QMessageBox>
#include "AboutDialog.h"
#include <QDir>

const QString Fbvisual::cacheFileName = "fbvisual_last_session.qds";

Fbvisual::Fbvisual(QWidget *parent) : QMainWindow(parent), fbManager(*this) {
	ui.setupUi(this);

	cacheDirName = QDir::homePath() + "/." + APPLICATION_NAME.toLower() + "/";

	connect(&fbManager, SIGNAL(managerLoggedIn()), this, SLOT(managerLoggedIn()));
	connect(&fbManager, SIGNAL(managerLoggedOut()), this, SLOT(managerLoggedOut()));

	ui.tableView->setModel(&friendsModel);
	ui.tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.tableView->setSelectionMode(QAbstractItemView::SingleSelection);

	connect(ui.tableView->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)),
			this, SLOT(changeCurrent(const QModelIndex&, const QModelIndex&)));
	ui.tableView->horizontalHeader()->setStretchLastSection(true);

	photosScene.setBackgroundBrush(QPixmap(":/gui/resources/sky_bg.jpg"));
	ui.graphicsView->setScene(&photosScene);
	photosScene.setSceneRect(-200, -250, 400, 500);

	ui.statusbar->showMessage(tr("Not logged in."));
	ui.actionLogin->setEnabled(true);
	ui.actionLogout->setDisabled(true);

	initMenu();
	loadModelFromCache();
}

Fbvisual::~Fbvisual() {
	delete next;
	delete prev;
}

void Fbvisual::initMenu() {
	connect(ui.actionLogin, SIGNAL(activated()), this, SLOT(login()));
	connect(ui.actionLogout, SIGNAL(activated()), this, SLOT(logout()));
	connect(ui.actionClearCache, SIGNAL(activated()), this, SLOT(deleteCache()));

	connect(ui.actionAbout, SIGNAL(activated()), this, SLOT(about()));
	connect(ui.actionExit, SIGNAL(activated()), this, SLOT(exit()));

	connect(ui.actionNext_Friend, SIGNAL(activated()), this, SLOT(nextFriend()));
	connect(ui.actionPrevious_Friend, SIGNAL(activated()), this, SLOT(prevFriend()));
	connect(ui.actionShow_me, SIGNAL(activated()), this, SLOT(showMe()));

	connect(ui.actionPin_image, SIGNAL(activated()), this, SLOT(pinImage()));
	connect(ui.actionHide_image, SIGNAL(activated()), this, SLOT(hideImage()));

	connect(ui.actionGraphic_Buttons, SIGNAL(toggled(bool)), this, SLOT(toggleGraphicButtons(bool)));
}

void Fbvisual::login() {
	ui.statusbar->showMessage(tr("Logging in ..."));
	fbManager.on_actionLogin_activated();
}

void Fbvisual::logout() {
	ui.statusbar->showMessage(tr("Logging out ..."));
	fbManager.on_actionLogout_activated();
}

void Fbvisual::managerLoggedIn() {
    QMessageBox msgbox;
    QString msg(tr("Logged in sucessfully"));
    msgbox.setText(msg);
    msgbox.exec();

    clear(); // czyscze cache bo udalo sie zalogowac wiec pobiore swieze

	ui.statusbar->showMessage(tr("Downloading friends"));

	ui.actionLogin->setDisabled(true);
	ui.actionClearCache->setDisabled(true);
	ui.actionLogout->setEnabled(true);

	fbManager.getOwner();
	fbManager.getFriends();
}

void Fbvisual::managerLoggedOut() {
    QMessageBox msgbox;
    msgbox.setText(tr("Logged out"));
    msgbox.exec();

    ui.statusbar->showMessage(tr("Not logged in."));

	ui.actionLogin->setEnabled(true);
	ui.actionClearCache->setEnabled(true);
	ui.actionLogout->setDisabled(true);
}

void Fbvisual::friendsLoaded(const QList<Friend*>& friends) {
	Friend* f;

	foreach(f, friends) { // nowe
		connect(&photoManager, SIGNAL(photoLoaded(QString, QPixmap*)), f, SLOT(photoLoaded(QString, QPixmap*)));
		photoManager.getPhotoByUrl(f->getUid(), f->getPhotoUrl());

		f->setContextMenu(ui.menuImage);
	}

	friendsModel.addFriends(friends);
	QString msg = tr("Ready, total friends: ");
	msg += QString::number(friends.size(), 10);
	ui.statusbar->showMessage(msg);
}

void Fbvisual::ownerLoaded(Friend* owner) {
	connect(&photoManager, SIGNAL(photoLoaded(QString, QPixmap*)), owner, SLOT(photoLoaded(QString, QPixmap*)));
	photoManager.getPhotoByUrl(owner->getUid(), owner->getPhotoUrl());
	friendsModel.setOwner(owner);

	owner->setContextMenu(ui.menuImage);

	showMe(); // pokaz mnie odrazu po zaladowaniu :)
}

void Fbvisual::changeCurrent(const QModelIndex& current, const QModelIndex& previous) {
	Friend* newFriend = friendsModel.getFriendByIndex(current);
	Friend* oldFriend = friendsModel.getFriendByIndex(previous);

	if(oldFriend == NULL)
		return;

	removeFriendsFormScene();
	if(newFriend != NULL && newFriend->scene() == NULL) {
		photosScene.addItem(newFriend);
		newFriend->setFlag(QGraphicsItem::ItemIsMovable);
		newFriend->setFlag(QGraphicsItem::ItemIsSelectable, true);
		newFriend->enterAnimation();
		ui.textAboutFriend->setText(newFriend->getAbout());
		ui.lineEditEmail->setText(newFriend->getContactEmail());
		ui.lineEditProfileURL->setText(newFriend->getProfileUrl());
	}
}

void Fbvisual::showMe() {
	Friend* owner = friendsModel.getOwner();
	if(owner == NULL || owner->scene() != NULL)
		return;
	removeFriendsFormScene();
	photosScene.addItem(owner);
	owner->setFlag(QGraphicsItem::ItemIsMovable);
	owner->setFlag(QGraphicsItem::ItemIsSelectable, true);
	owner->enterAnimation();
	ui.textAboutFriend->setText(owner->getAbout());
	ui.lineEditEmail->setText(owner->getContactEmail());
	ui.lineEditProfileURL->setText(owner->getProfileUrl());
}

void Fbvisual::closeEvent(QCloseEvent *event) {
	saveModelToCache();
    QMainWindow::closeEvent(event);
}

void Fbvisual::exit() {
//	saveModelToCache(); // close wywoluje close event :)
	close();
}

void Fbvisual::about() {
	AboutDialog aboutDialog;
	aboutDialog.exec();
}

void Fbvisual::nextFriend() {
	 if(friendsModel.rowCount() == 0) {
		 return;
	 }

	 QModelIndex prevIndex, nextIndex;
	 QItemSelection selection, prevSelection;
	 QItemSelectionModel* selectionModel = ui.tableView->selectionModel();
	 QModelIndexList list = selectionModel->selectedRows();

	 if(list.empty()) {
		 nextIndex = friendsModel.index(0, 0, QModelIndex());
		 prevIndex = QModelIndex();
		 selection.select(nextIndex, nextIndex);
		 selectionModel->select(selection, QItemSelectionModel::Select | QItemSelectionModel::Rows);
		 changeCurrent(nextIndex, nextIndex);
	 }
	 else if(list.front().row() < friendsModel.rowCount()-1) {
		 prevIndex = list.front();
		 nextIndex = friendsModel.index(prevIndex.row()+1, prevIndex.column(), QModelIndex());

		 selection.select(nextIndex, nextIndex);
		 selectionModel->select(selection, QItemSelectionModel::Select | QItemSelectionModel::Rows);

		 prevSelection.select(prevIndex, prevIndex);
		 selectionModel->select(prevSelection, QItemSelectionModel::Toggle | QItemSelectionModel::Rows);
		 changeCurrent(nextIndex, prevIndex);
	 }
}

void Fbvisual::prevFriend() {
	 if(friendsModel.rowCount() == 0) {
		 return;
	 }

	 QModelIndex prevIndex, nextIndex;
	 QItemSelection selection, prevSelection;
	 QItemSelectionModel* selectionModel = ui.tableView->selectionModel();
	 QModelIndexList list = selectionModel->selectedRows();

	 if(list.empty()) {
		 nextIndex = friendsModel.index(friendsModel.rowCount()-1, 0, QModelIndex());
		 prevIndex = QModelIndex();
		 selection.select(nextIndex, nextIndex);
		 selectionModel->select(selection, QItemSelectionModel::Select | QItemSelectionModel::Rows);
		 changeCurrent(nextIndex, nextIndex);
	 }
	 else if(list.front().row() > 0) {
		 prevIndex = list.front();
		 nextIndex = friendsModel.index(prevIndex.row()-1, prevIndex.column(), QModelIndex());

		 selection.select(nextIndex, nextIndex);
		 selectionModel->select(selection, QItemSelectionModel::Select | QItemSelectionModel::Rows);

		 prevSelection.select(prevIndex, prevIndex);
		 selectionModel->select(prevSelection, QItemSelectionModel::Toggle | QItemSelectionModel::Rows);
		 changeCurrent(nextIndex, prevIndex);
	 }
}

void Fbvisual::toggleGraphicButtons(bool checked) {
	if(checked) {
		next = new ChangeFriendButton();
		prev = new ChangeFriendButton(true);
		connect(next, SIGNAL(pressed()), this, SLOT(nextFriend()));
		connect(prev, SIGNAL(pressed()), this, SLOT(prevFriend()));
		next->setPos(130, 180);
		prev->setPos(-130, 180);
		next->scale(0.75, 0.75);
		prev->scale(0.75, 0.75);
		next->setZValue(10);
		prev->setZValue(10);
		photosScene.addItem(next);
		photosScene.addItem(prev);
	} else {
		photosScene.removeItem(next);
		photosScene.removeItem(prev);
		delete next;
		delete prev;
	}
}

void Fbvisual::saveModelToCache() {
	if(friendsModel.rowCount() > 0) {
		friendsModel.saveModel(cacheDirName + cacheFileName);
	}
}

void Fbvisual::loadModelFromCache() {
	if(QFile::exists(cacheDirName + cacheFileName)) {
		friendsModel.loadModel(cacheDirName + cacheFileName);
		QList<Friend*> friends = friendsModel.getFriends();
		Friend* f;
		friends.push_back(friendsModel.getOwner());

		foreach(f, friends) {
			connect(&photoManager, SIGNAL(photoLoaded(QString, QPixmap*)), f, SLOT(photoLoaded(QString, QPixmap*)));
			photoManager.getPhotoByUrl(f->getUid(), f->getPhotoUrl());

			f->setContextMenu(ui.menuImage);
		}

		showMe();
	}
}

void Fbvisual::clear() {
    friendsModel.clearModel();
    ui.lineEditEmail->setText("");
    ui.lineEditProfileURL->setText("");
    ui.textAboutFriend->setText("");
}

void Fbvisual::deleteCache() {
	clear(); // wywalamy z pamieci

	// wywalamy z dysku
	QFileInfo fileInfo;
	QFileInfoList fileList = QDir(cacheDirName).entryInfoList();

	foreach(fileInfo, fileList) {
		QFile::remove(fileInfo.fileName());
	}
}

void Fbvisual::removeFriendsFormScene() {
	Friend* oldFriend;
	QList<QGraphicsItem *> listToLeave = photosScene.items();
	QGraphicsItem* item;
	foreach(item, listToLeave) {
		if((oldFriend = dynamic_cast<Friend*>(item)) != NULL && !oldFriend->getPined()) {
			oldFriend->leaveAnimation();
		}
	}
}


void Fbvisual::pinImage() {
	if (photosScene.selectedItems().isEmpty())
		 return;

	QGraphicsItem* selectedItem = photosScene.selectedItems().first();
	Friend* f;
	if((f = dynamic_cast<Friend*>(selectedItem)) != NULL) {
		f->setPined(true);
	}
}

void Fbvisual::hideImage() {
	if (photosScene.selectedItems().isEmpty())
		 return;

	QGraphicsItem* selectedItem = photosScene.selectedItems().first();
	Friend* f;
	if((f = dynamic_cast<Friend*>(selectedItem)) != NULL) {
		f->setPined(false);
		f->leaveAnimation();
	}
}
