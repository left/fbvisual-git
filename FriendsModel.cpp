#include "FriendsModel.h"

#include <QDebug>
#include <QFile>
#include <QtGui/QApplication>

FriendsModel::FriendsModel() : owner(NULL) { /* empty */ }

FriendsModel::~FriendsModel() {
	deleteFriends();
}

int FriendsModel::rowCount(const QModelIndex &parent) const {
	Q_UNUSED(parent);
	return friends.size();
}

int FriendsModel::columnCount(const QModelIndex & parent) const {
	// TODO ogarnac to
	Q_UNUSED(parent);
	return 3;
}

QVariant FriendsModel::data(const QModelIndex &index, int role) const {
	if (!index.isValid())
		return QVariant();

	if (index.row() >= friends.size())
		return QVariant();

	if (role == Qt::DisplayRole) {
		switch(index.column()) {
			case 0:
				return friends.at(index.row())->getName();
			case 1:
				return friends.at(index.row())->getSurname();
			case 2:
				return friends.at(index.row())->getBirthDate();
		}
	}
	return QVariant();
}

QVariant FriendsModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch(section) {
			case 0:
				return tr("Name");
			case 1:
				return tr("Surname");
			case 2:
				return tr("Birhday");
			default:
				return QString();
        }
    }
    else
        return QString("%1").arg(section+1);
}

void FriendsModel::addFriends(const QList<Friend*>& newFriends) {
	int currentSize = friends.size();
	int newFriendsSize = newFriends.size();
	Friend* f;

	beginInsertRows(QModelIndex(), currentSize-1, currentSize+newFriendsSize-2);
	foreach(f, newFriends) {
		qDebug() << "Friend was added: " << f->getSurname();
		friends.push_back(f);
	}
	endInsertRows();
}

void FriendsModel::clearModel() {
	int size = friends.size();
	beginRemoveRows(QModelIndex(), 0, size-1);
	deleteFriends();
	endRemoveRows();
	emit(dataChanged(index(0, 0, QModelIndex()), index(0, size-1, QModelIndex())));
}

Friend* FriendsModel::getFriendByIndex(const QModelIndex& index) const {
	if (!index.isValid())
		return NULL;

	if (index.row() >= friends.size())
		return NULL;

	return friends.at(index.row());
}

void FriendsModel::deleteFriends() {
	delete owner;
	owner = NULL;
	while(!friends.empty()) {
		Friend *f = friends.back();
		friends.pop_back();
		delete f;
	}
}

bool FriendsModel::saveModel(QString fileName) {
	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly))
		return false;

	qDebug() << "Friends model is being saved...";
	QDataStream outStream(&file);
	outStream.setVersion(QDataStream::Qt_4_6);

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor)); // set wait cursor during saving
	outStream << owner;
	outStream << friends;
	QApplication::restoreOverrideCursor();
	file.close();
	return true;
}

bool FriendsModel::loadModel(QString fileName) {
	QFile file(fileName);
	if(!file.open(QIODevice::ReadOnly))
		return false;

	qDebug() << "Friends model is being loaded...";
	QDataStream inStream(&file);
	inStream.setVersion(QDataStream::Qt_4_6);
	QList<Friend*> friendsList;

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor)); // set wait cursor during loading
	inStream >> owner;
	inStream >> friendsList;
	QApplication::restoreOverrideCursor();
	file.close();
	addFriends(friendsList);
	return true;
}

QList<Friend*> FriendsModel::getFriends() const {
	return friends;
}


Friend* FriendsModel::getOwner() const {
	return owner;
}

void FriendsModel::setOwner(Friend* modelOwner) {
	owner = modelOwner;
}
