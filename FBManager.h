#ifndef FBMANAGER_H_
#define FBMANAGER_H_

#include <QObject>

#include <QFacebookConnect/fbsession.h>
#include <QFacebookConnect/fblogindialog.h>
#include "Friend.h"

//#include "fbvisual.h"

class Fbvisual; // forward declaration

/**
 * Class is resposible for connection with facebook.
 */
class FBManager : public QObject{
	Q_OBJECT
private:
	FBSession* iFBSession;
	FBLoginDialog* iLoginDialog;
	Fbvisual& fbVisual;

	QString userId;

public:
	FBManager(Fbvisual& fbVisual);
	~FBManager();

	void getFriends() const;
	void getOwner() const;

public slots:
	// TODO sloty czy nie sloty, oto jest pytanie
	void on_actionLogin_activated();
	void on_actionLogout_activated();

private slots:
	void sessionDidLogin(FBUID aUid);
	void sessionDidLogout();
	void requestFailedWithFacebookError(const FBError& aError);

	void friendsLoaded(const QVariant& aContainer);
	void ownerLoaded(const QVariant& aContainer);

signals:
	void managerLoggedIn();
	void managerLoggedOut();
};

#endif /* FBMANAGER_H_ */
