#ifndef PHOTOMANAGER_H_
#define PHOTOMANAGER_H_

#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QPixmap>
#include <QImageReader>
#include <QHash>

/**
 * Klasa odpowiada za pobieranie zdjec przez http.
 */
class PhotoManager : public QObject {
	Q_OBJECT
private:
	QNetworkAccessManager networkManager;
	QHash<QNetworkReply*, QString> replyOnIdMap;
	QString cachePath;

	void saveToDisk(QPixmap* pixmap, QString fileName);

public:
	PhotoManager();
	~PhotoManager();

	void getPhotoByUrl(const QString& id, const QString& url);

signals:
	void photoLoaded(QString id, QPixmap* pixmap);

private slots:
	void requestFinished(QNetworkReply* reply);
};


#endif /* PHOTOMANAGER_H_ */
