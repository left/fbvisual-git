#ifndef FRIEND_H_
#define FRIEND_H_

#include <QGraphicsObject>
#include <QPixmap>
#include <QString>
#include <QParallelAnimationGroup>
#include <QtGui/QMenu>

/**
 * Klasa reprezentuje przyjaciela.
 *
 * Moze byc wyswietlana na scenenie w graphic view.
 */
class Friend : public QGraphicsObject {
	Q_OBJECT
	Q_PROPERTY(QPointF pos READ pos WRITE setPos) // ta wlasciowsc animujemy
	Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity) // ta wlasciowsc animujemy

private:
	QString uid;
	QString name;
	QString surname;
	QString birthDate;
	QString contactEmial;
	QString about;
	QString photoUrl;
	QString profileUrl;
	QPixmap* photo;

	QParallelAnimationGroup* animationGroup;
	QMenu* contextMenu;
	bool pined;

public:
	Friend();
	Friend(const Friend&);
	~Friend();
	Friend& operator=(const Friend&);

	// iherited
	QRectF boundingRect() const;

	/**
	 * Rysuje zdjecie przyjaciela dodajac mala ramke oraz cien.
	 */
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	/**
	 * Nacisniecie na zdjecie powoduje przerwanie animacji (ze wzgledu na przezroczytsosc - zeby nie szarpalo)
	 */
	void mousePressEvent(QGraphicsSceneMouseEvent* event);

	/**
	 * Obsluga rzadania menu kontekstowego.
	 */
	void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

	/**
	 * Animacja wejsciea na scene.
	 */
	void enterAnimation();
	/**
	 * Animacja wyjscia ze sceny
	 */
	void leaveAnimation();

	QString getUid() const;
	QString getBirthDate() const;
	QString getName() const;
	QString getPhotoUrl() const;
	QString getSurname() const;
	QString getContactEmail() const;
	QString getAbout() const;
	QString getProfileUrl() const;
	bool getPined() const;
	void setBirthDate(const QString& birthDate);
	void setName(const QString& name);
	void setPhotoUrl(const QString& photoUrl);
	void setSurname(const QString& surname);
	void setUid(const QString& uid);
	void setContactEmail(const QString& email);
	void setAbout(const QString& about);
	void setProfileUrl(const QString& profileUrl);
	void setPined(bool pin);

	void setContextMenu(QMenu* menu);

public slots:
	void photoLoaded(QString uid, QPixmap* pixmap);
	void leaveAnimationFinished();

private:
	void deleteAnimationGroup();

};

QDataStream &operator<<(QDataStream &out, const Friend* f);
QDataStream &operator>>(QDataStream &in, Friend*& f);

#endif /* FRIEND_H_ */
