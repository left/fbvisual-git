#include "Friend.h"

#include <QDebug>
#include <QPainter>
#include <QPropertyAnimation>
#include <QGraphicsScene>
#include <QGraphicsSceneContextMenuEvent>

Friend::Friend() {
	photo = NULL;
	animationGroup = NULL;
	contextMenu = NULL;
	pined = false;
}

Friend::Friend(const Friend& f) : QGraphicsObject() {
	uid = f.uid;
	name = f.name;
	surname = f.surname;
	birthDate = f.birthDate;
	photoUrl = f.photoUrl;
	photo = new QPixmap(*f.photo);
	contextMenu = f.contextMenu;
	animationGroup = NULL;
	pined = false;
//	setFlag(QGraphicsItem::ItemIsMovable);
}

Friend::~Friend() {
	delete photo;
	deleteAnimationGroup();
}

Friend& Friend::operator=(const Friend& f) {
	if(this == &f) {
		return *this;
	}
	uid = f.uid;
	name = f.name;
	surname = f.surname;
	birthDate = f.birthDate;
	photoUrl = f.photoUrl;
	photo = new QPixmap(*f.photo);
	return *this;
}

QRectF Friend::boundingRect() const {
	if(photo != NULL) {
		return QRectF(-(photo->width()>>1), -(photo->height()>>1), photo->width() + 6, photo->height() + 6);
	}
	else {
		return QRectF();
	}
}

void Friend::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
	if(photo != NULL) {
		QColor shadow(70, 70, 70, 100);
		QPen pen(shadow, 1);
		painter->setPen(pen);
		painter->setBrush(shadow);
		painter->drawRoundRect(-(photo->width()>>1) + 6, -(photo->height()>>1) + 6, photo->width(), photo->height(), 2, 2);
		painter->drawPixmap(-(photo->width()>>1), -(photo->height()>>1), *photo);
		painter->setPen(QPen(Qt::black, 3));
		painter->setBrush(Qt::NoBrush);
		painter->drawRoundRect(-(photo->width()>>1), -(photo->height()>>1), photo->width(), photo->height(), 2, 2);
	}
}

void Friend::mousePressEvent(QGraphicsSceneMouseEvent* event) {
	animationGroup->stop();
	scene()->clearSelection();
	setOpacity(1);
}

void Friend::contextMenuEvent(QGraphicsSceneContextMenuEvent *event) {
	if(contextMenu == NULL) {
		return;
	}
    scene()->clearSelection();
    setSelected(true);
    contextMenu->exec(event->screenPos());
}

void Friend::enterAnimation() {
	deleteAnimationGroup();
	animationGroup = new QParallelAnimationGroup();

	QPropertyAnimation* animation = new QPropertyAnimation(this, "opacity");
	animation->setDuration(2000);

	animation->setKeyValueAt(0, 0);
//	animation->setKeyValueAt(0.25, 0);
	animation->setKeyValueAt(1, 1);

	animationGroup->addAnimation(animation);

	animation = new QPropertyAnimation(this, "pos");
	animation->setDuration(1700);

	animation->setKeyValueAt(0, QPointF(-1000, -1000));
	animation->setKeyValueAt(0.37, QPointF(-700, -700));
	animation->setKeyValueAt(0.58, QPointF(-150, -100));
	animation->setKeyValueAt(0.79, QPointF(-100, 100));
	animation->setKeyValueAt(1, QPointF(0, 0));
	animation->setEasingCurve(QEasingCurve::OutBack);

	animationGroup->addAnimation(animation);

	animationGroup->start();
}

void Friend::leaveAnimation() {
	deleteAnimationGroup();
	animationGroup = new QParallelAnimationGroup();

	QPropertyAnimation* animation = new QPropertyAnimation(this, "opacity");
	animation->setDuration(700);

	animation->setKeyValueAt(0, 1);
	animation->setKeyValueAt(1, 0);

	animationGroup->addAnimation(animation);

	animation = new QPropertyAnimation(this, "pos");
	animation->setDuration(700);

	animation->setKeyValueAt(0, QPointF(pos().x(), pos().y()));
	animation->setKeyValueAt(0.33, QPointF(pos().x()+150, pos().y()+-120));
	animation->setKeyValueAt(0.66, QPointF(pos().x()+300, pos().y()+150));
	animation->setKeyValueAt(1, QPointF(pos().x()+500, pos().y()+500));
	animation->setEasingCurve(QEasingCurve::InBack);

	animationGroup->addAnimation(animation);

	animationGroup->start();

	connect(animationGroup, SIGNAL(finished()), this, SLOT(leaveAnimationFinished()));
}

void Friend::leaveAnimationFinished() {
	if(scene() != NULL)
		scene()->removeItem(this);
}

void Friend::photoLoaded(QString uid, QPixmap* pixmap) {
	if(this->uid == uid) {
		qDebug() << "Photo was loaded for: " << this->getSurname();
		photo = pixmap;
		sender()->disconnect(this, SLOT(photoLoaded(QString, QPixmap*)));
	}
}

QString Friend::getUid() const {
	return uid;
}

QString Friend::getBirthDate() const {
	return birthDate;
}

QString Friend::getName() const {
	return name;
}

QString Friend::getPhotoUrl() const {
	return photoUrl;
}

QString Friend::getSurname() const {
	return surname;
}

QString Friend::getContactEmail() const {
	return contactEmial;
}

QString Friend::getAbout() const {
	return about;
}

QString Friend::getProfileUrl() const {
	return profileUrl;
}

bool Friend::getPined() const {
	return pined;
}

void Friend::setBirthDate(const QString& birthDate) {
	this->birthDate = birthDate;
}

void Friend::setName(const QString& name) {
	this->name = name;
}

void Friend::setPhotoUrl(const QString& photoUrl) {
	this->photoUrl = photoUrl;
}

void Friend::setSurname(const QString& surname) {
	this->surname = surname;
}

void Friend::setUid(const QString& uid) {
	this->uid = uid;
}

void Friend::setContactEmail(const QString& email) {
	this->contactEmial = email;
}

void Friend::setAbout(const QString& about) {
	this->about = about;
}

void Friend::setProfileUrl(const QString& profileUrl) {
	this->profileUrl = profileUrl;
}

void Friend::setContextMenu(QMenu* menu) {
	contextMenu = menu;
}

void Friend::setPined(bool pin) {
	pined = pin;
}

void Friend::deleteAnimationGroup() {
	if(animationGroup != NULL) {
		while(animationGroup->animationCount() != 0) {
			delete animationGroup->animationAt(0);
		}
		delete animationGroup;
		animationGroup = NULL;
	}
}

QDataStream &operator<<(QDataStream &out, const Friend* f) {
	out << f->getName() << f->getSurname() << f->getBirthDate()
			<< f->getAbout() << f->getContactEmail() << f->getPhotoUrl()
			<< f->getProfileUrl() << f->getUid();
	return out;
}

QDataStream &operator>>(QDataStream &in, Friend*& f) {
	QString tmp;
	f = new Friend();

	in >> tmp;
	f->setName(tmp);
	in >> tmp;
	f->setSurname(tmp);
	in >> tmp;
	f->setBirthDate(tmp);

	in >> tmp;
	f->setAbout(tmp);
	in >> tmp;
	f->setContactEmail(tmp);
	in >> tmp;
	f->setPhotoUrl(tmp);

	in >> tmp;
	f->setProfileUrl(tmp);
	in >> tmp;
	f->setUid(tmp);
	return in;
}
