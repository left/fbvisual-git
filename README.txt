Do poprawnej kompilacji projektu potrzebna jest biblioteka QFacebookConnect.
Mozna ja pobrac pod tym adresem:

http://gitorious.org/qfacebookconnect

Kompilacja przebiega bez klopotu. Jednak moze byc problem z linkowaniem do projektu.

Biblioteka kompiluje sie domsylnie jako dynamiczna (shared object). U mnie wystapowal blad w czasie uruchamiania
programow wykorzystujacych biblioteke (z nie wiadomych przyczyn krzyczal ze system nie jest 32 bitowy).

Pomoga ustawienie bibloteki na kompilacje do obiektu statycznego: CONFIG += staticlib w pliku libqfacebook.pro.

Po komilacji nalezy dobrze ustawic sciezki do biblioteki albo zmienic #include w PAIN'owym projekcie.
