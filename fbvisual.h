#ifndef FBVISUAL_H
#define FBVISUAL_H

#include <QtGui/QMainWindow>
#include <QString>
#include "ui_fbvisual.h"
#include "FBManager.h"
#include "FriendsModel.h"
#include "PhotoManager.h"
#include "ChangeFriendButton.h"

/** Nazwa zaplikacja */
const QString APPLICATION_NAME = "FbVisual";

/**
 * Glowna klasa plikacji
 */
class Fbvisual : public QMainWindow {
    Q_OBJECT
private:
    /** Gui */
    Ui::FbvisualClass ui;
    /** Model przyjaciol oparty o tabele */
    FriendsModel friendsModel;
    /** Scena do wyswietlania zdjec */
    QGraphicsScene photosScene;
    /** Wskazniki na graficzne przyciski (nastepne/poprzednie zdjecie) */
    ChangeFriendButton* next;
    ChangeFriendButton* prev;

    /** Obiekt odpowiedzialny za  komunikacje z Facebookiem */
    FBManager fbManager;
    /** Obiekt odpowiedzialny za ladowanie i cachowanie zdjec (z http) */
    PhotoManager photoManager;

    /** Nazwa pliku dla cache modelu przyjaciol */
    static const QString cacheFileName;
    /** Nazwa folderu dla cache */
    QString cacheDirName;

    /**
     * Zapisuje model do pliku o ile nie jest pusty.
     */
    void saveModelToCache();

    /**
     * Wczytuje model z pliku o ile plik istnieje.
     */
    void loadModelFromCache();

    /**
     * Czysci gui i scene z przyjaciol
     */
    void clear();

    /**
     * Wywoluje clear() i usuwa caly cache na dysku.
     */
    void removeFriendsFormScene();

    /**
     * Laczy sygnaly menu ze slotami aplikacji.
     */
    void initMenu();

private slots:

	void login();
	void logout();

	/**
	 * Wywolane gdy udalo sie zalogowac do facebooka.
	 */
	void managerLoggedIn();

	/**
	 * Wywolane gdy udadlo sie wylogowac z facebooka.
	 */
	void managerLoggedOut();

	/**
	 * Polaczone z selecionModel dla tableView.
	 */
	void changeCurrent(const QModelIndex& current, const QModelIndex& previous);

	/**
	 * Wyswietlenie nastepnego przyjaciela.
	 */
	void nextFriend();

	/**
	 * Wyswietlenie poprzedniego przyjaciela.
	 */
	void prevFriend();

	/**
	 * Wyswietlenie tego kto sie zalgoowal do facebooka (wlasciciela sesji)
	 */
	void showMe();

	/**
	 * Przytwierdzenie obrazka do tla.
	 */
	void pinImage();

	/**
	 * Usuniecie obrazka ze sceny.
	 */
	void hideImage();

	/**
	 * Wyswietla dialog about
	 */
	void about();

	/**
	 * Wychodzi z aplikacji
	 */
	void exit();

	/**
	 * Usuwa cache.
	 */
    void deleteCache();

    /**
     * Wyswietlenie przyciskow (polaczone z checked menu)
     */
	void toggleGraphicButtons(bool checked);

protected:
	/**
	 * Przed zamknieciem zapisuje do cache.
	 */
	void closeEvent(QCloseEvent *event);

public:
    Fbvisual(QWidget* parent = 0);
    ~Fbvisual();

	void friendsLoaded(const QList<Friend*>& firendsList);
	void ownerLoaded(Friend* owner);
};

#endif // FBVISUAL_H
