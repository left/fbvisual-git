#include "FriendsGraphicsView.h"

FriendsGraphicsView::FriendsGraphicsView(QWidget* parrent) : QGraphicsView(parrent) {
	// TODO Auto-generated constructor stub

}

void FriendsGraphicsView::resizeEvent(QResizeEvent *event) {
	QGraphicsView::resizeEvent(event);
	if(rect().width() < sceneRect().width() || rect().height() < sceneRect().height())
		fitInView(sceneRect(), Qt::KeepAspectRatio);
}
