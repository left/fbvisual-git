#ifndef FRIENDSMODEL_H_
#define FRIENDSMODEL_H_

#include <QAbstractTableModel>
#include <QList>

#include "Friend.h"

/**
 * Model przyjaciol oparty o tabele.
 */
class FriendsModel : public QAbstractTableModel {
	Q_OBJECT
private:
	/** Wlasciciel modelu. */
	Friend* owner;

	/** Przyjaciele wlasciciela. */
	QList<Friend*> friends;

	void deleteFriends();

public:
	FriendsModel();
	virtual ~FriendsModel();

	// zapis odczyt modelu  pliku
	bool saveModel(QString fileName);
	bool loadModel(QString fileName);

	Friend* getOwner() const;
	void setOwner(Friend* modelOwner);
	void addFriends(const QList<Friend*>& friends);
	void clearModel();
	Friend* getFriendByIndex(const QModelIndex& index) const;
	QList<Friend*> getFriends() const;

	// inherited - implementuje interfej modelu
	int rowCount(const QModelIndex& parent = QModelIndex()) const;
	int columnCount(const QModelIndex& parent = QModelIndex()) const;
	QVariant data(const QModelIndex& index, int role) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
};

#endif /* FRIENDSMODEL_H_ */
