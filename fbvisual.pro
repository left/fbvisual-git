TEMPLATE = app
TARGET = fbvisual
QT += core \
    gui \
    network \
    webkit \
    xml
HEADERS += FriendsGraphicsView.h \
    ChangeFriendButton.h \
    AboutDialog.h \
    PhotoManager.h \
    FBManager.h \
    Friend.h \
    FriendsModel.h \
    fbvisual.h
SOURCES += FriendsGraphicsView.cpp \
    ChangeFriendButton.cpp \
    AboutDialog.cpp \
    PhotoManager.cpp \
    FBManager.cpp \
    Friend.cpp \
    FriendsModel.cpp \
    main.cpp \
    fbvisual.cpp
FORMS += AboutDialog.ui \
    fbvisual.ui
RESOURCES += resources.qrc
LIBS += -lqfacebookconnect
