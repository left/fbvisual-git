#ifndef FRIENDSGRAPHICSVIEW_H_
#define FRIENDSGRAPHICSVIEW_H_

#include <QGraphicsView>

/*
 *
 */
class FriendsGraphicsView : public QGraphicsView {
	Q_OBJECT
public:
	FriendsGraphicsView(QWidget* parrent);
protected:
     void resizeEvent(QResizeEvent *event);
};

#endif /* FRIENDSGRAPHICSVIEW_H_ */
