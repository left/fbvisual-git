#ifndef CHANGEFRIENDBUTTON_H_
#define CHANGEFRIENDBUTTON_H_

#include <QGraphicsWidget>

/**
 * Klasa reprezentuje przycisk do zmieniania przyjaciol o jeden.
 */
class ChangeFriendButton : public QGraphicsWidget {
	Q_OBJECT
private:
	QPixmap pixmap;
public:
	ChangeFriendButton(bool rotate = false);
	~ChangeFriendButton();

	QRectF boundingRect() const;
	QPainterPath shape() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

 signals:
	 void pressed();

 protected:
	 void mousePressEvent(QGraphicsSceneMouseEvent *);
	 void mouseReleaseEvent(QGraphicsSceneMouseEvent *);
};

#endif /* CHANGEFRIENDBUTTON_H_ */
